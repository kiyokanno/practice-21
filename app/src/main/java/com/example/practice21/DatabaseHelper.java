package com.example.practice21;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;

/*Create a bridge between android and SQLite.
To perform basic SQL operations It needs to extend SQLiteOpenHelper class.*/
public class DatabaseHelper extends SQLiteOpenHelper {

    // First thing, a database needs to be created in onCreate().
    public SQLiteDatabase db01;

    private static final int DATABASE_VERSION = 1;//A version of SQLite.
    private static final String DATABASE_NAME = "account.db"; // Make sure adding ".db".
    private static final String TABLE_NAME = "accounts";// somehow not recognised. This may causing login validation issue.
    private static final String COLUMN_ID = "id";// ID for datatable for finding each data.
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "password";
    //private static final String TABLE_CREATE = "CREATE TABLE contacts (id integer PRIMARY KEY NOT NULL , " + " username text NOT NULL, email text NOT NULL, password text NOT NULL );";

    //Constructor of this class.
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Create data table by SQL statement.
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + " ( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_USERNAME + " VARCHAR, " + COLUMN_EMAIL + " VARCHAR, " + COLUMN_PASSWORD + " VARCHAR);");
        //db.execSQL(TABLE_CREATE);//Creating a table.
        this.db01 = db;
    }

    //This method receives the object "contact" and insert the data contents to database.
    public void insertContact(Contact contact) {
        db01 = this.getWritableDatabase();
        ContentValues values = new ContentValues(); //ContentValues class for storing a set of values so that ContentResolver class can provide this application access to the content model.

        String query = "SELECT * FROM " + TABLE_NAME; //SQL statement: selecting all the existing data columns from the data table.
        Cursor cursor = db01.rawQuery(query, null);//Create cursor.
        int count = cursor.getCount();//Cursor can location the position of data columns by ID.

        //Insert entered user data to data table.
        values.put(COLUMN_ID, count);
        values.put(COLUMN_USERNAME, contact.getUsername());
        values.put(COLUMN_EMAIL, contact.getEmail());
        values.put(COLUMN_PASSWORD, contact.getPassword());

        db01.insert(TABLE_NAME, null, values);//Insert table name and ID number.
        db01.close();// Do not forget closing database.
    }

    // An username-and-password search method. Pass data to MainActivity.
    public String searchPassword(String username) {
        db01 = this.getReadableDatabase();
        String query = "SELECT username, password FROM " + TABLE_NAME;// Select username column and password column from the table..
        Cursor cursor = db01.rawQuery(query, null);//Return
        String u, p; //u for username, p for password.
        p = "Not found.";
        if (cursor.moveToFirst()) {//Move the cursor to the first row.
            do {
                u = cursor.getString(0); // The index of 0 should be the selected column "ID" on the table. Get ID from ID number 0.

                if (u.equals(username)) {//Validate the entered username on the login layout with the username on the data table.
                    p = cursor.getString(1);// The index of 1 should be the selected column "password" on the data table. Get the password of the username on which matches with the username on the data table.
                    break;//Get out of this if statement.
                }
            }
            while (cursor.moveToNext());//Move the cursor to the next row which is relative to the current position on the data table.
        }

        return p;//Return the password of the matched username on the data table.
    }//end searchPassword(String username)

    // Update SQLite Version.
/*   This method is called when the database version changed.
     This method drops tables/ add tables or to do anything else
     in order to upgrade to the new schema version.*/
    @Override
    public void onUpgrade(SQLiteDatabase db01, int oldVersion, int newVersion) {
        String query = "DROP TABLE IF EXISTS " + TABLE_NAME; // Make sure correct SQL query here.
        db01.execSQL(query);
        this.onCreate(db01);
    }

}//end public class DatabaseHelper extends SQLiteOpenHelper

