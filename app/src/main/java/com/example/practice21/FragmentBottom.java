package com.example.practice21;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentBottom extends Fragment {

    private EditText editTextInput;
    //private long startTimeInMillis;
    //private long timeLeftMillis = startTimeInMillis;
    private TextView txtVwCountdown;
    private Button btnSet;
    private ImageButton btnStartPause;
    private ImageButton btnReset;

    private CountDownTimer countDownTimer;

    private boolean timerRunning;
    private long startTimeInMillis;
    private long timeLeftMillis = startTimeInMillis;

    public FragmentBottom() {
        //Remember: Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Declare a local variable of View class in order to access ViewGroup of Activity.
        View view = inflater.inflate(R.layout.fragment_fragment_bottom, container, false);

        //Provide member variable with content via id.
        editTextInput = view.findViewById(R.id.editTxt_inputNumb);
        txtVwCountdown = view.findViewById(R.id.countdown_text);

        btnSet = view.findViewById(R.id.btn_setNumb);
        btnStartPause = view.findViewById(R.id.button_start);
        btnReset = view.findViewById(R.id.button_reset);

        //Set button
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = editTextInput.getText().toString();//Get and pass the entered number by user to the input variable.
                if (input.length() == 0) { //Check whether user entered number or not.
                    //Pop up a reminder message.
                    Toast.makeText(getActivity(), "Minute has to be entered", Toast.LENGTH_SHORT).show();//"getActivity()" is necessary to be used since fragmentBottom class is a subclass of activity.
                    return;// Prevent program executing and comes below this if block.
                }

                //Pause and input milliseconds.
                long millisInput = Long.parseLong(input) * 60000;//Parse the content of editTextInput. 60000 = a minute.
                if (millisInput == 0) {//Prevent user from only entering 0s.
                    Toast.makeText(getActivity(), "Enter only positive number", Toast.LENGTH_SHORT).show();
                    return;
                }

                setTime(millisInput);// Pass the entered value to run setTime method.
                editTextInput.setText("");//Set the EditText field empty.
            }
        }); //end Set button

        //Start & Pause button
        btnStartPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timerRunning) { // Initialised as true.
                    pauseTimer();//Runs the method to pause the timer.
                } else {
                    startTimer();//Runs the actual function for running the time.
                }
            }
        });

        //Reset button
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTimer();// Runs the method to reset the timer.
            }
        });

        updateCountDownTxt();// Start countdown the time.

        return view;// DO not delete!

    }//end public View onCreateView()

    //Set a new time and a countdown time.
    private void setTime(long milliseconds) {
        startTimeInMillis = milliseconds;//Remember startTimeInMillis is no longer a constant!
        resetTimer();
    }

    // startTimer()
    private void startTimer() {
        countDownTimer = new CountDownTimer(timeLeftMillis, 1000) {
            @Override //Runs the main codes of timer.
            public void onTick(long millisUntilFinished) {
                timeLeftMillis = millisUntilFinished;//600000
                updateCountDownTxt();//Start counting down 10 min.
            }

            @Override // This method runs when the time is reached 0.
            public void onFinish() {
                timerRunning = false;
                btnStartPause.setEnabled(true);
                btnReset.setVisibility(View.VISIBLE);//Show the stop circle image.
            }
        }.start();

        timerRunning = true;
        btnStartPause.setPressed(false);
        editTextInput.setVisibility(View.INVISIBLE);//Set editTextInput invisible if the time is running.
        btnSet.setVisibility(View.INVISIBLE);//Set the set button invisible if the time is running.
        btnReset.setVisibility(View.INVISIBLE);//Set the reset button invisible if the time is running.
    }

    //pauseTimer()
    private void pauseTimer() {
        countDownTimer.cancel();//Pause the timer at where the time is running.
        timerRunning = false;

        btnStartPause.setEnabled(true);//Show the stop circle image.
        btnReset.setVisibility(View.VISIBLE);//Set the cancel button visible.
        editTextInput.setVisibility(View.INVISIBLE);//Set editTextInput invisible if the time is running.
        btnSet.setVisibility(View.INVISIBLE);//Ser the set button invisible if the time is running.
    }

    //resetTimer()
    private void resetTimer() {
        timeLeftMillis = startTimeInMillis; // Applies the initial value.
        updateCountDownTxt();//Run the timer.

        btnStartPause.setEnabled(true);//Shows the stop circle image.
        btnReset.setVisibility(View.INVISIBLE);// Make the reset button invisible.
        editTextInput.setVisibility(View.VISIBLE);//Make editTextInput visible if the time is running.
        btnSet.setVisibility(View.VISIBLE);//Make the set button visible if the time is running.
    }

    // Formula of time calculator
    private void updateCountDownTxt() {
        int hours = (int) (timeLeftMillis / 1000) / 3600; //1hr is 3600 seconds.
        int minutes = (int) ((timeLeftMillis / 1000) % 3600) / 60;//Subtract the time exceeded to get the fracture amount of time.
        int seconds = (int) (timeLeftMillis / 1000) % 60;

        String timeLeftFormatted;
        if (hours > 0) {//Change the format of showing the time entered by user.
            timeLeftFormatted = String.format(Locale.getDefault(),
                    "%d:%02d:%02d", hours, minutes, seconds);//Formats the order of time in hours, mintues and seconds. Limit the numbers of digit to one digit for hours.
        } else {
            timeLeftFormatted = String.format(Locale.getDefault(),
                    "%02d:%02d", minutes, seconds);//Formats the order of the timer in mintues and seconds.
        }
        txtVwCountdown.setText(timeLeftFormatted);//Show the appropriate order of time.
    }
}
