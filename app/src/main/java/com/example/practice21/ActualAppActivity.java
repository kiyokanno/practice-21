package com.example.practice21;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;

import com.google.android.material.navigation.NavigationView;

public class ActualAppActivity extends AppCompatActivity {

    private DrawerLayout drawer;// DrawerLayout contains nav_header and drawer_menu.xml
    DatabaseHelper signoutDatabase = new DatabaseHelper(this);// Access to DataHelper.java via an instant variable.
    TextView txtViewUsernameDisplay; //In order to display username on navigation header.

    @Override // Create hamburger icon as the layout opens.
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acutal_app);

        //Toolbar Inflater
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);//The hamburger is created on the tool bar.

        // Drawer Inflater.
        drawer = findViewById(R.id.drawer_layout);

        //Get drawer over the drawer_layout.
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);//Add the toggle variable to the list of listeners which notifies of drawer events.
        toggle.syncState();//Synchronise the indicator with the state of drawer_layout variable(via an instance drawer of DrawerLayout class).

        //Inflate NavigationView
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //Inflate header view
        View header = navigationView.getHeaderView(0);

        txtViewUsernameDisplay = (TextView) header.findViewById(R.id.TxtVw_username_display);//Remember TxtVw_username_display is in nav_header.xml contained in "drawer"
        Intent intent = getIntent();//Initialise Intent locally.
        String strUsername = intent.getStringExtra("Username");//Receive String value from MainActivity via Intent.
        txtViewUsernameDisplay.setText(strUsername);

        FragmentBottom fragmentBottom = new FragmentBottom();//Initialise a Fragment class FragmentBottom.

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container_bottom, fragmentBottom)
                .commit();
    }//end onCreate(Bundle savedInstanceState)

    /*This block of codes opens the navigation menu bar*/
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {// Drawer menu appears from left.
            drawer.closeDrawer(GravityCompat.START); // Close Drawer menu.
        } else {
            super.onBackPressed();
        }
    }

    /*This code below works: Only drops out of SQLight. It does not refresh MainActivity*/
    /*Logout on clicking the logout label(as an item) on the drawer_menu.xml*/
    /*Must access to MenuItem in order to refer to the id of nav_logout.*/
    public void signOut(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            signoutDatabase.db01.execSQL("DROP TABLE accoutns");//Accessing the exact SQLight database via an instant variable of DatabaseHelper class.
            Intent intent = new Intent(ActualAppActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

}//end ActualAppActivity class
