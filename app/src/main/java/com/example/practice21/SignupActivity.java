package com.example.practice21;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {

    DatabaseHelper dbHelper = new DatabaseHelper(this);//Accessing DatabaseHelper class.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
    }

    /**
     * Call when the user taps the Signup button on the Signup screen
     */
    public void signUpOnSignupScreen(View view) {

        if (view.getId() == R.id.btn_Signup2) {
            // Initialising variables in order to find empty variables so that an empty EditText can be identified to pop up a warning Toast message after.
            String userNameString = "";
            String emailString = "";
            String password1String = "";
            String password2String = "";

            EditText userName = (EditText) findViewById(R.id.txtInputEditTxt_Username);
            EditText email = (EditText) findViewById(R.id.txtInputEditTxt_Email);
            EditText password1 = (EditText) findViewById(R.id.editTxt_password1);
            EditText password2 = (EditText) findViewById(R.id.editTxt_passward2);

            userNameString = userName.getText().toString();
            emailString = email.getText().toString();
            password1String = password1.getText().toString();
            password2String = password2.getText().toString();

            //Find empty string.
            if (userNameString.length() == 0 || emailString.length() == 0 || password1String.length() == 0 || password2String.length() == 0) {
                //Pop up a warning message!
                Toast WarningSignup = Toast.makeText(SignupActivity.this, "Some information is missing. Fill out all.", Toast.LENGTH_SHORT);
                WarningSignup.show();
            } else if (!password1String.equals(password2String))//Validate passwords.
            {
                //Pop up a warning message!
                Toast passwordWarningSignup = Toast.makeText(SignupActivity.this, "Passwords don't match.", Toast.LENGTH_SHORT);
                passwordWarningSignup.show();
            } else {
                //Insert the login details in database.
                Contact contact = new Contact();
                contact.setUsername(userNameString);
                contact.setEmail(emailString);
                contact.setPassword(password1String);

                dbHelper.insertContact(contact);// Passing the object "contact" to DatabaseHelper class to insert entered data to database.

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }
        }
    }//end signUpOnSignupScreen(View view)

    // Open the login layout by using Intent.
    public void cancel(View view) {
        if (view.getId() == R.id.txtViw_Cancel) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
