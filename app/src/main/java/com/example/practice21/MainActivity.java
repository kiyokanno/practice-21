package com.example.practice21;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.renderscript.ScriptGroup;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper helper = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Called when the user taps the Login button on the login screen
     */
    public void loginPassword(View view) {

        if (view.getId() == R.id.btn_LogIn) {

            String usernameStr = "";
            String passwordStr = "";

            EditText editTxt_username = (EditText) findViewById(R.id.TxtInputEditTxt_Username);
            EditText editTxt_password = (EditText) findViewById(R.id.editTxt_passward0);

            usernameStr = editTxt_username.getText().toString();
            passwordStr = editTxt_password.getText().toString();

            String passwordHelper = helper.searchPassword(usernameStr);// Receiving username and password from DatabaseHelper class.

            // Check whether both string variables(username and password) are entered or not.
            if (usernameStr.isEmpty() || passwordStr.isEmpty())// "Object.isEmpty()" in stead of "Object.length() == 0"): isEmpty is more appropriate/adequate for just finding out empty variables.
            {
                //Pop up a warning message!
                Toast loginWarning = Toast.makeText(MainActivity.this, "Username and password must be entered.", Toast.LENGTH_SHORT);
                loginWarning.show();
            } else if (passwordStr.equals(passwordHelper)) // Check the validity of username and password.
            {
                Intent intent = new Intent(this, ActualAppActivity.class); // Intent constructor takes two parameters.
                intent.putExtra("Username", usernameStr);//Passing a string value which converted from EditText to a string value.
                startActivity(intent);

                editTxt_username.setText("");//Clear EditText to get ready for next login after logging out.
                editTxt_password.setText("");//Clear EditText to get ready for next login after logging out.

            } else {
                //Pop up a warning message!
                Toast usernamePasswordWarning = Toast.makeText(MainActivity.this, "Username and password don't match.", Toast.LENGTH_SHORT);
                usernamePasswordWarning.show();
            }
        }

    }//end loginPassword(View view)

    /**
     * Called when the user taps the Signup button on the login screen
     */
    public void signUpFirstScreen(View view) {
        Intent intent = new Intent(this, SignupActivity.class); //Via Intent, shows SinupActivity layout/xml.
        startActivity(intent);
    }

}//end public class MainActivity extends AppCompatActivity

